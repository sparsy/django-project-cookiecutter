#!/usr/bin/env bash

# clean old build
rm -rf dist {{ cookiecutter.project_name }}.pyz

# include the dependencies from `pip-compile`
python3 -m pip install -r requirements.txt --target dist/ --upgrade

# specify which files to be included in the build
cp -r \
-t dist \
{{ cookiecutter.project_name }} manage.py

# finally, build!
shiv --site-packages dist --compressed -p '/usr/bin/env python3' -o {{ cookiecutter.project_name }}.pyz -e manage:main
