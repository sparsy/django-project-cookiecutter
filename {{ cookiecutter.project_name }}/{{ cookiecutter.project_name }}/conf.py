import base64
import os

from goodconf import GoodConf, Value


def get_base_dir():
    return os.getcwd()


class Config(GoodConf):
    DEBUG = Value(
        default=True,
        help="Toggle debugging."
    )
    DATABASE_URL = Value(
        default='sqlite://db.sqlite3',
        help="Database connection."
    )
    SECRET_KEY = Value(
        default=base64.b64encode(os.urandom(60)).decode(),
        help="Used for cryptographic signing. "
        "https://docs.djangoproject.com/en/2.0/ref/settings/#secret-key"
    )
    STATIC_URL = Value(default='/static/')
    STATIC_ROOT = Value(
        default=os.path.join(get_base_dir(), "static"),
        help="STATIC_ROOT absolute path for referencing."
    )
    MEDIA_URL = Value(default='/media/')
    MEDIA_ROOT = Value(
        default=os.path.join(get_base_dir(), "media"),
        help="MEDIA_ROOT absolute path for referencing."
    )
    EMAIL_HOST = Value(default='smtp.gmail.com')
    EMAIL_PORT = Value(default=465)
    EMAIL_HOST_USER = Value(default='HOST_USER')
    EMAIL_HOST_PASSWORD = Value(default='HOST_PASSWORD')
    EMAIL_USE_TLS = Value(default=True)
    SENDER_EMAIL = Value(default='REPLAY_EMAIL')
    DEFAULT_FROM_EMAIL = Value(default='FROM_EMAIL')


config = Config(
    default_files=[
        "{{ cookiecutter.project_name }}.yaml"
    ]
)
