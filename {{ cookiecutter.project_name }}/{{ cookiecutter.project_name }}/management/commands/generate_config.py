import os
from django.core.management.base import BaseCommand
from django.conf import settings
from {{ cookiecutter.project_name }}.conf import config


class Command(BaseCommand):
    help = 'Generate a configuration file structure'

    def _ask_confirm(self, question, default=None):
        result = input(f'{question} ')
        if not result and default is not None:
            return default
        while len(result) < 1 or result[0].lower() not in 'yn':
            result = input('Please answer yes or no: ')
        return result[0].lower() == 'y'

    def handle(self, *args, **kwargs):
        config_path = os.path.join(
            settings.BASE_DIR,
            '{{ cookiecutter.project_name }}.yaml'
        )
        content = config.generate_yaml()

        if os.path.exists(config_path):
            if not self._ask_confirm('Configuration already exists, override?'):
                return

        with open(config_path, 'w') as f:
            f.write(content)
