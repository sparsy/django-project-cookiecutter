import os
from setuptools import setup, find_packages

from {{ cookiecutter.project_name }} import __version__

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='{{ cookiecutter.project_name }}',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    description='{{ cookiecutter.project_description }}',
    author='{{ cookiecutter.author }}',
    author_email='{{ cookiecutter.email }}',
    install_requires=[
        'Django',
        'django-extensions',
        'django-debug-toolbar',
        'django-admin-env-notice',
        'dj-database-url',
        'dj-cmd',
        'ipython',
        'ipdb',
        'pudb',
        'pip-tools',
        'django-webserver[pyuwsgi]',
        'goodconf[yaml]',
        'shiv'
    ],
)
