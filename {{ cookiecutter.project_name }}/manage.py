#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
from {{ cookiecutter.project_name }}.conf import config


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{{ cookiecutter.project_name }}.settings')
    config.django_manage()


if __name__ == '__main__':
    main()
